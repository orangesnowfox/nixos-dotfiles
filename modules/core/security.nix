{ pkgs, config, lib, ... }:
with lib;
with builtins;
let
  cfg = config.sys.security;
  # desktopMode = if ((length config.sys.graphics.desktopProtocols) > 0) then true else false;
in
{
  config = {
    security.sudo.enable = false;
    security.doas.enable = true;
  };
}
