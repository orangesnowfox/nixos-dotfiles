{ lib, config, pkgs, ... }:
{
  config = {
    # programs all of Skyler's computers probably need at some point.
    environment.systemPackages = with pkgs; [
      neovim
      wget
      firefox
      alacritty
      git
      git-crypt
      accountsservice
      # todo: move this to "backup" rather than "disk/backup"
      (lib.mkIf config.sys.disk.backup.enable my.backup)
      # compsize build is currently broken
      # compsize
      file
      unzip
    ];

    # for auto-vendoring completions
    programs.fish.enable = true;
  };
}
