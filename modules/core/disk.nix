{ config, pkgs, lib, ... }:
with lib;
with builtins;
let
  cfg = config.sys.disk;
  # theoretically bcachefs or zfs also exist.
  supportsSubvolumes = (cfg.layout == "btrfs");
in
{
  options.sys.disk = {
    layout = mkOption {
      type = types.enum [ "btrfs" ];
      description = "This is the layout of the disk used by the system.";
      default = "btrfs";
    };

    beesDedup = mkEnableOption "bees deduplication";

    root = {
      label = mkOption {
        type = types.str;
        description = "label for the root partition.";
        default = "nixos";
      };
    };

    backup =
      {
        enable = mkEnableOption "a backup drive";
        label = mkOption {
          type = types.str;
          description = "label for backup drive.";
          default = "backup";
        };
      };

    boot =
      {
        label = mkOption {
          type = types.str;
          description = "label for the boot partition.";
          default = "boot";
        };
      };

    home = {
      enable = mkEnableOption "a seperate home partition";
      label = mkOption {
        type = types.str;
        description = "label for the home partition.";
      };

    };
  };

  config = {
    assertions = [
      {
        assertion = cfg.beesDedup -> (cfg.layout == "btrfs");
        message = "beesd can only be used with btrfs";
      }
    ];

    fileSystems."/boot" = {
      device = "/dev/disk/by-label/${cfg.boot.label}";
      fsType = "vfat";
    };

    fileSystems."/" = {
      device = "/dev/disk/by-label/${cfg.root.label}";
      fsType = cfg.layout;
      options = [ (mkIf (cfg.layout == "btrfs") "compress-force=zstd:2") ];
    };

    # fixme: this is kinda sorta really a mess.
    fileSystems."/media/backup/borg" = mkIf (cfg.backup.enable) {
      device = "/dev/disk/by-label/${cfg.backup.label}";
      fsType = cfg.layout;
      options = [
        (mkIf supportsSubvolumes "subvol=@borg")
        (mkIf (cfg.layout == "btrfs") "compress-force=zstd:5")
        "nofail"
      ];
    };

    # fixme: this is kinda sorta really a mess.
    # todo: move this mount point to `snapshots/`,
    # just put "home" snapshots in `snapshots/home/` (regular dir) on `@snapshots` subvolume
    fileSystems."/media/backup/snapshots-home" = mkIf (cfg.backup.enable) {
      device = "/dev/disk/by-label/${cfg.backup.label}";
      fsType = cfg.layout;
      options = [
        (mkIf supportsSubvolumes "subvol=@snapshots-home")
        (mkIf (cfg.layout == "btrfs") "compress-force=zstd:8")
        "nofail"
      ];
    };

    # fixme: this is kinda sorta really a mess.
    fileSystems."/home" = mkIf (supportsSubvolumes || cfg.home.enable) {
      device =
        if cfg.home.enable then "/dev/disk/by-label/${cfg.home.label}"
        else "/dev/disk/by-label/${cfg.root.label}";
      fsType = cfg.layout;
      options = [
        (mkIf supportsSubvolumes "subvol=@home")
        (mkIf (cfg.layout == "btrfs" && cfg.home.enable) "compress-force=zstd:4")
      ];
    };

    # fixme: this is kinda sorta really a mess.
    # todo: handle the proper way of doing this if on the same disk
    # requires a `@snapshots-home` if it's the same disk
    fileSystems."/snapshots/@home" = mkIf (supportsSubvolumes && cfg.home.enable) {
      device = "/dev/disk/by-label/${cfg.home.label}";
      fsType = cfg.layout;
      options = [ "subvol=@snapshots" ];
    };

    swapDevices = [ ];

    services.beesd.filesystems = mkIf (cfg.beesDedup)
      {
        root = {
          spec = "LABEL=${cfg.root.label}";
          hashTableSizeMB = 512;
          verbosity = "warning";
        };

        home = (mkIf cfg.home.enable {
          spec = "/home";
          hashTableSizeMB = 3072;
          verbosity = "warning";
        });

        # fixme: dedup on backup degrading system perf
        # backup = (mkIf cfg.backup.enable {
        #  spec = "LABEL=${cfg.backup.label}";
        #  hashTableSizeMB = 3072;
        #  verbosity = "warning";
        # });
      };

    # hack, put this elsewhere, it just *depends* on this.
    # also add an enable/disable option.
    services.cron = mkIf (supportsSubvolumes && cfg.home.enable && cfg.backup.enable) ({
      enable = true;
      systemCronJobs = [
        "0 0 * * * root ${pkgs.my.backup}/bin/backup"
      ];
    });
  };
}
