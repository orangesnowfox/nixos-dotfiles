{ config, pkgs, lib, ... }:
with lib;
with builtins;
let
  cfg = config.sys;
in
rec {
  imports = [
    ./nixos.nix
    ./security.nix
    ./disk.nix
    ./packages.nix
  ];

  options.sys = {
    biosType = mkOption {
      type = types.enum [ "efi" ];
      description = "Specify the bios type of the machine";
    };

    bootloader = mkOption {
      type = types.enum [ "grub" "systemd-boot" ];
      description = "The boot loader used to boot the system";
      default = "systemd-boot";
    };

    cpu = {
      type = mkOption {
        type = types.enum [ "intel" "amd" ];
        description = "Type of cpu the system has in it";
      };
    };

    memtest86 = {
      enable = mkOption {
        default = false;
        type = types.bool;
        description = ''
          NOTE: currently only works for systemd-boot. 
          Make MemTest86 available from the boot menu. MemTest86 is a
          program for testing memory.  MemTest86 is an unfree program, so
          this requires <literal>allowUnfree</literal> to be set to
          <literal>true</literal>.
        '';
      };
    };
  };

  config = {
    hardware.firmware = with pkgs;[ firmwareLinuxNonfree ];
    hardware.enableAllFirmware = true;
    hardware.enableRedistributableFirmware = true;
    # nixpkgs.config.allowUnfree = true;

    boot.kernelParams = [
      (mkIf (cfg.cpu.type == "intel") "intel_pstate=active")
    ];

    environment.systemPackages = with pkgs; [
      (mkIf (cfg.cpu.type == "amd") microcodeAmd)
      (mkIf (cfg.cpu.type == "intel") microcodeIntel)
    ];

    boot.loader.systemd-boot.enable = cfg.bootloader == "systemd-boot";
    boot.loader.systemd-boot.editor = false;
    boot.loader.systemd-boot.memtest86.enable = (cfg.bootloader == "systemd-boot") && cfg.memtest86.enable;
    boot.loader.efi.canTouchEfiVariables = cfg.bootloader == "systemd-boot";
  };
}
