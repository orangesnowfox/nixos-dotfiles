{
  description = "Skyler Ross' nixos config";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";

    # follow the leader
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.92.0.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ { self, nixpkgs, home-manager, lix-module, ... }:
    let
      system = "x86_64-linux";

      # cool girl's pro tip:
      # this basically is `lib = nixpkgs.lib;`
      # why not just use that? Dunno. Guess it's DRYer.
      inherit (nixpkgs) lib;

      util = import ./lib { inherit system pkgs home-manager lix-module lib; overlays = (pkgs.overlays); };

      inherit (util) user;
      inherit (util) host;

      pkgs = import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
        overlays = [
          (self: super: {
            my = import ./pkgs {
              inherit pkgs;
            };
          })
        ];
      };

    in
    {
      packages."${system}" = pkgs;

      homeManagerConfigurations = {
      };

      installMedia = {
        mini = host.mkISO {
          name = "nixos";
          kernelPackage = pkgs.linuxPackages;
          initrdMods = [ "xhci_pci" "ahci" "usb_storage" "sd_mod" "nvme" "usbhid" ];
          kernelMods = [ "kvm-intel" "kvm-amd" ];
          kernelParams = [ ];
          roles = [ "core" "user" ];
        };
      };

      nixosConfigurations = {
        koko = host.mkHost
          {
            roles = [ "core" "desktop-xorg" "graphics-amd" "games" "multi-media" ];
            name = "koko";
            NICs = [
              "enp6s0"
              #              "wlp5s0" 
            ];
            #           wifi = [ "wlp5s0" ];
            initrdMods = [ "xhci_pci" "ahci" "nvme" "usbhid" "usb_storage" "sd_mod" "amdgpu" ];
            kernelMods = [ "kvm-intel" ];
            kernelParams = [ ];
            kernelPackage = pkgs.linuxPackages_xanmod_latest;
            users = [{
              name = "sr";
              groups = [ "wheel" "networkmanager" "dialout" "libvirtd" ];
              uid = 1000;
              shell = pkgs.nushell;
            }];
            homeManagerUsers = {
              sr = user.mkNixosHMUser {
                username = "sr";
                roles = [ "core" "desktop/sway" "fish" "nushell" "neovim" "desktop/i18n" ];
              };
            };
            cfg = {
              sys.biosType = "efi";
              sys.cpu.type = "intel";
              sys.disk = {
                layout = "btrfs";
                home.enable = true;
                home.label = "nixos-home";
                backup.enable = true;
                beesDedup = false;
              };
              sys.memtest86.enable = true;
            };
          };
      };
    };
}
