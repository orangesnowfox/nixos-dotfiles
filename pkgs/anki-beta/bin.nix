{ fetchurl, stdenv, lib, buildFHSEnv, appimageTools, writeShellScript, anki, undmg, zstd, cacert, commandLineArgs ? [ ] }:

let
  # Tracks latest beta, but latest beta is latest stable, soooo.
  pname = "anki-beta-bin";
  # Update hashes for both Linux and Darwin!
  version = "25.01beta1";
  # for whatever reason the "version" in the actual files is this but the version is `beta<x>`.
  release-version = "25.01";

  sources = {
    linux = fetchurl {
      url = "https://github.com/ankitects/anki/releases/download/${version}/anki-${release-version}-linux-qt6.tar.zst";
      hash = "sha256-mh9s85kvZ2HsKceL0g+dpVN+H05TXH5jBjnMf02mLD0=";
    };

    # For some reason anki distributes completely separate dmg-files for the aarch64 version and the x86_64 version
    darwin-x86_64 = fetchurl {
      url = "https://github.com/ankitects/anki/releases/download/${version}/anki-${release-version}-mac-intel-qt6.dmg";
      sha256 = lib.fakeSha256;
    };
    darwin-aarch64 = fetchurl {
      url = "https://github.com/ankitects/anki/releases/download/${version}/anki-${release-version}-mac-apple-qt6.dmg";
      sha256 = lib.fakeSha256;
    };
  };

  unpacked = stdenv.mkDerivation {
    inherit pname version;

    nativeBuildInputs = [ zstd ];
    src = sources.linux;

    installPhase = ''
      runHook preInstall

      xdg-mime () {
        echo Stubbed!
      }
      export -f xdg-mime

      PREFIX=$out bash install.sh

      runHook postInstall
    '';
  };

  meta = with lib; {
    inherit (anki.meta) license homepage description longDescription;
    platforms = [ "x86_64-linux" "x86_64-darwin" "aarch64-darwin" ];
    maintainers = with maintainers; [ mahmoudk1000 atemu ];
  };

  passthru = { inherit sources; };

  fhsEnvAnki = buildFHSEnv (appimageTools.defaultFhsEnvArgs // {
    inherit pname version;
    
    profile = ''
      # anki vendors QT and mixing QT versions usually causes crashes
      unset QT_PLUGIN_PATH
      # anki uses the system ssl cert, without it plugins do not download/update
      export SSL_CERT_FILE="${cacert}/etc/ssl/certs/ca-bundle.crt"
    '';

    # Dependencies of anki
    targetPkgs = pkgs: (with pkgs; [
      xorg.libxkbfile
      xorg.libxshmfence
      xcb-util-cursor-HEAD
      krb5
      zstd
    ]);

    runScript = writeShellScript "anki-wrapper.sh" ''
      exec ${unpacked}/bin/anki ${ lib.strings.escapeShellArgs commandLineArgs } "$@"
    '';

    extraInstallCommands = ''
      ln -s ${pname} $out/bin/anki

      mkdir -p $out/share
      cp -R ${unpacked}/share/applications \
        ${unpacked}/share/man \
        ${unpacked}/share/pixmaps \
        $out/share/
    '';

    inherit meta passthru;
  });
in

if stdenv.isLinux then fhsEnvAnki
else
  stdenv.mkDerivation {
    inherit pname version passthru;

    src = if stdenv.isAarch64 then sources.darwin-aarch64 else sources.darwin-x86_64;

    nativeBuildInputs = [ undmg ];
    sourceRoot = ".";

    installPhase = ''
      mkdir -p $out/Applications/
      cp -a Anki.app $out/Applications/
    '';

    inherit meta;
  }
