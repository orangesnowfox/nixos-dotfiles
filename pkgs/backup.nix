{ substituteAll, bash, btrfs-progs, writeScriptBin, config, lib, ... }:
let
  srcFile = substituteAll {
    src = ./backup.sh;
    bash = "${bash}/bin/bash";
    btrfs = "${btrfs-progs}/bin/btrfs";
  };
  src = builtins.readFile srcFile;
in
writeScriptBin "backup" src
