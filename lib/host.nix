{ system, pkgs, home-manager, lix-module, lib, user, ... }:
with builtins;
{
  # shamely copied from wiltaylor
  # if he ever exports this as a flake, I'll probably just use that. 
  mkISO = { name, initrdMods, kernelMods, kernelParams, kernelPackage, roles }:
    let
      roles_mods = (map (r: mkRole r) roles);

      mkRole = name: import (../roles/iso + "/${name}");
    in
    lib.nixosSystem {
      inherit system;

      specialArgs = { };

      modules = [
        lix-module.nixosModules.default
        {
          # I have no need for these modules so far, so...
          # imports = [ ../modules ] ++ roles_mods;

          imports = roles_mods;

          networking.hostName = "${name}";
          networking.useDHCP = false;

          boot.initrd.availableKernelModules = initrdMods;
          boot.kernelModules = kernelMods;

          boot.kernelParams = kernelParams;
          boot.kernelPackages = kernelPackage;

          nixpkgs.pkgs = pkgs;

          # This value determines the NixOS release from which the default
          # settings for stateful data, like file locations and database versions
          # on your system were taken. It‘s perfectly fine and recommended to leave
          # this value at the release version of the first install of this system.
          # Before changing this value read the documentation for this option
          # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
          system.stateVersion = "22.05"; # Did you read the comment?
        }
      ];
    };

  mkHost =
    { name
    , initrdMods
    , kernelMods
    , kernelParams
    , kernelPackage
    , NICs
    , roles
    , users
    , homeManagerUsers
    , wifi ? [ ]
    , cfg ? { }

    }:
    let
      networkCfg = listToAttrs (map
        (n: {
          name = "${n}";
          value = { };
          value = { useDHCP = true; };
        })
        NICs);

      mkRole = name: import (../roles/system + "/${name}");
      roles_mods = (map (r: mkRole r) roles);
      sys_users = (map (u: user.mkSystemUser u) users);
    in
    lib.nixosSystem {
      inherit system;
      modules = [
        cfg
        home-manager.nixosModules.home-manager
        lix-module.nixosModules.default
        {
          imports = [ ../modules ] ++ roles_mods ++ sys_users;

          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users = homeManagerUsers;

          networking.hostName = "${name}";
          networking.interfaces = networkCfg;
          networking.wireless.interfaces = wifi;

          # The global useDHCP flag is deprecated, therefore explicitly set to false here.
          # Per-interface useDHCP will be mandatory in the future, so, ensure that this
          # replicates the default behaviour.
#         networking.networkmanager.enable = true;
          networking.useDHCP = false;

          boot.initrd.availableKernelModules = initrdMods;
          boot.kernelModules = kernelMods;
          boot.kernelParams = kernelParams;
          boot.kernelPackages = kernelPackage;

          nixpkgs.pkgs = pkgs;
          system.stateVersion = "22.05"; # Did you read the comment?
        }
      ];
    };
}
