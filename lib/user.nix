{ pkgs, home-manager, lib, system, overlays, ... }:
with builtins;
{
  mkHMUser = { username, roles }:
    let
      mkRole = name: import (../roles/user + "/${name}");
      modRoles = map (r: mkRole r) roles;
    in
    home-manager.lib.homeManagerConfiguration {
      pkgs = pkgs;
      modules = [
        {
          nixpkgs.config.allowUnfree = true;
          nixpkgs.overlays = overlays;
          home.stateVersion = "22.05";
          home.username = username;
          home.homeDirectory = "/home/${username}";
          systemd.user.startServices = "sd-switch";
          imports = modRoles;
        }
      ];
    };
  mkNixosHMUser = { username, roles }:
  let
    mkRole = name: import (../roles/user + "/${name}");
    modRoles = map (r: mkRole r) roles;
  in {
    home.stateVersion = "22.05";
    home.username = username;
    home.homeDirectory = "/home/${username}";
    systemd.user.startServices = "sd-switch";
    imports = modRoles;
  };

  mkSystemUser = { name, groups, uid, shell, ... }:
    {
      users.users."${name}" = {
        name = name;
        isNormalUser = true;
        isSystemUser = false;
        extraGroups = groups;
        uid = uid;
        initialPassword = "Password1";
        shell = shell;
      };
    };
}
