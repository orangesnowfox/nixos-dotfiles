#!/bin/sh

flake_path="$(dirname "$(realpath "$0")")"

nix build "${flake_path}#homeManagerConfigurations.sr.activationPackage"
# other users ...
"${flake_path}/result/activate"
