#!/usr/bin/env bash

set -e
set -o pipefail
shopt -s inherit_errexit

profile=/nix/var/nix/profiles/system
installBootloader=
pathToConfig="$1"

nix-env -p "$profile" --set "$pathToConfig"

cmd=(
    "systemd-run"
    "-E" "LOCALE_ARCHIVE" # Will be set to new value early in switch-to-configuration script, but interpreter starts out with old value
    "-E" "NIXOS_INSTALL_BOOTLOADER=$installBootloader"
    "--collect"
    "--no-ask-password"
    "--pipe"
    "--quiet"
    "--same-dir"
    "--service-type=exec"
    "--unit=nixos-rebuild-switch-to-configuration"
    "--wait"
    "$pathToConfig/bin/switch-to-configuration"
    "switch"
)

"${cmd[@]}"
