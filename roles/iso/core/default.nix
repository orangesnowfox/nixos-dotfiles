{ config, pkgs, lib, modulesPath, ... }:
{
  system.stateVersion = "22.05";

  nix = {
    extraOptions = "experimental-features = nix-command flakes";
    gc = {
      automatic = true;
      options = "--delete-older-than 5d";
    };
  };

  # Make this config a iso config
  imports = [ "${modulesPath}/installer/cd-dvd/iso-image.nix" "${modulesPath}/profiles/all-hardware.nix" ];

  isoImage.makeEfiBootable = true;
  isoImage.makeUsbBootable = true;

  boot.blacklistedKernelModules = [ "sp5100_tco" ];

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";
  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";
  console.font = lib.mkDefault "${pkgs.terminus_font}/share/consolefonts/ter-u28n.psf.gz";
  hardware.enableRedistributableFirmware = lib.mkDefault true;

  boot.supportedFilesystems = [ "btrfs" "vfat" "xfs" ];

  environment.pathsToLink = [ "/libexec" ];

  isoImage.isoBaseName = "nixos";
  isoImage.edition = "flakes";
  isoImage.isoName = "${config.isoImage.isoBaseName}-${config.system.nixos.label}-${config.isoImage.edition}-${pkgs.stdenv.hostPlatform.system}.iso";

  networking.networkmanager.enable = true;

  programs.fish.enable = true;
  programs.zsh.enable = true;

  environment.systemPackages = with pkgs; [
    wget
    pciutils
    curl
    unzip
    file
    zip
    p7zip
    git
    git-crypt
    unrar
    nix-bundle
    microcodeIntel
    acpi
    nix-index
    btrfs-progs
    neovim
    nvme-cli
    firmwareLinuxNonfree
    testdisk # useful for repairing boot problems
    efibootmgr
    efivar
    parted
    gptfdisk
    ddrescue
    mkpasswd # for generating password files
    (pkgs.runCommand "neovim-alias" { } ''
      mkdir -p $out/bin
      ln ${pkgs.neovim}/bin/nvim $out/bin/vim -sf
      ln ${pkgs.neovim}/bin/nvim $out/bin/vi -sf
    '')
  ];

  security.sudo.enable = false;
  security.doas.enable = true;
}
