module eza-completions {
    def "nu-complete eza maybe" []  {
        ["always", "auto", "never"]
    }

    def "nu-complete eza absolute" [] {
        ["on", "follow", "off"]
    }

    def "nu-complete eza sort" [] {
        [
            "name",
            "Name",
            "extension",
            "Extension",
            "size",
            "type",
            "created",
            "modified",
            "accessed",
            "changed",
            "inode",
            "none"
        ]
    }

    def "nu-complete eza time" [] {
        ["modified", "accessed", "created"]
    }

    def "nu-complete eza time-style" [] {
        ["default", "iso", "long-iso", "full-iso", "relative"]
    }

    def "nu-complete eza color-scale" [] {
        ["all", "age", "size"]
    }

    def "nu-complete eza color-scale-mode" [] {
        ["gradient", "fixed"]
    }

    # a modern replacement for ls
    export extern "eza" [
        ...file: path                                                     # Path to list
        --help(-?)                                                        # Show list of command-line options
        --version(-v)                                                     # Show version of eza
        --oneline(-1)                                                     # Display one entry per line
        --long(-l)                                                        # Display extended file metadata as a table
        --grid(-G)                                                        # Display entries as a grid (default)
        --across(-x)                                                      # Sort the grid across, rather than downwards
        --recurse(-R)                                                     # Recurse into directories
        --tree(-T)                                                        # Recurse into directories as a tree
        --dereference(-X)                                                 # Dereference symbolic links when displaying information
        --classify(-F): string@"nu-complete eza maybe"                    # Display type indicator by file names (always, auto, never)
        --color: string@"nu-complete eza maybe"                           # When to use terminal colors (always, auto, never)
        --colour: string@"nu-complete eza maybe"                          # When to use terminal colors (always, auto, never), same as color.
        --color-scale: string@"nu-complete eza color-scale"               # Highlight levels of field distinctly.  Use comma(,) separated list of all, age, size
        --colour-scale: string@"nu-complete eza color-scale"              # Highlight levels of field distinctly.  Use comma(,) separated list of all, age, size, same as color-scale.
        --color-scale-mode: string@"nu-complete eza color-scale-mode"     # Use gradient or fixed colors in --color-scale.
        --colour-scale-mode: string@"nu-complete eza color-scale-mode"    # Use gradient or fixed colors in --color-scale. Same as color-scale-mode
        --icons: string@"nu-complete eza maybe"                           # Display icons next to file names
        --no-quotes                                                       # Don't quote file names with spaces
        --hyperlink                                                       # Display entries as hyperlinks
        --absolute: string@"nu-complete eza absolute"                     # Display entries with their absolute path (on, follow, off)
        --follow-symlinks                                                 # Drill down into symbolic links that point to directories
        --width(-w): int                                                  # Set screen width in columns
        --all(-a)                                                         # Show hidden and 'dot' files. Use this twice to also show the '.' and '..' directories
        --almost-all(-A)                                                  # Equivalent to --all; included for compatibility with `ls -A`
        --list-dirs(-d)                                                   # List directories as files; don't list their contents
        --only-dirs(-D)                                                   # List only directories
        --only-files(-f)                                                  # List only files
        --show-symlinks                                                   # Explicitly show symbolic links (for use with --only-dirs | --only-files
        --no-symlinks                                                     # Do not show symbolic links
        --level(-L): int                                                  # Limit the depth of recursion
        --reverse(-r)                                                     # Reverse the sort order
        --sort(-s): string@"nu-complete eza sort"                         # Set which field to sort by
        --group-directories-first                                         # List directories before other files
        --group-directories-last                                          # List directories after other files
        --ignore-glob(-I): string                                         # Glob patterns (pipe-separated) of files to ignore
        --git-ignore                                                      # Ignore files mentioned in '.gitignore'
        --binary(-b)                                                      # List file sizes with binary prefixes
        --bytes(-B)                                                       # List file sizes in bytes, without any prefixes
        --group(-g)                                                       # List each file's group
        --smart-group                                                     # Only show group if it has a different name from owner
        --header(-h)                                                      # Add a header row to each column
        --links(-H)                                                       # List each file's number of hard links
        --inode(-i)                                                       # List each file's inode number
        --mounts(-M)                                                      # Show mount details (Linux and Mac only)
        --numeric(-n)                                                     # List numeric user and group IDs
        --flags(-O)                                                       # List file flags (Mac, BSD, and Windows only)
        --blocksize(-S)                                                   # Show size of allocated file system blocks
        --time(-t): string@"nu-complete eza time"                         # Which timestamp field to list (modified, accessed, created)
        --modified(-m)                                                    # Use the modified timestamp field
        --accessed(-u)                                                    # Use the accessed timestamp field
        --created(-U)                                                     # Use the created timestamp field
        --changed                                                         # Use the changed timestamp field
        --time-style: string@"nu-complete eza time-style"                 # How to format timestamps (default, iso, long-iso, full-iso, relative, or a custom style '+<FORMAT>')
        --total-size                                                      # Show the size of a directory as the size of all files and directories inside (unix only)
        --octal-permissions(-o)                                           # List each file's permission in octal format
        --no-permissions                                                  # Suppress the permissions field
        --no-filesize                                                     # Suppress the filesize field
        --no-user                                                         # Suppress the user field
        --no-time                                                         # Suppress the time field
        --stdin                                                           # Read file names from stdin, one per line or other separator specified in environment
        --git                                                             # List each file's Git status, if tracked or ignored
        --no-git                                                          # Suppress Git status (always overrides --git, --git-repos, --git-repos-no-status)
        --git-repos                                                       # List root of git-tree status
        --git-repos-no-status                                             # List each git-repos branch name (much faster)
        --extended(-@)                                                    # List each file's extended attributes and sizes
        --context(-Z)                                                     # List each file's security context
    ]
}

use eza-completions *;
