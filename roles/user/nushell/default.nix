{ pkgs, config, lib, ... }:
{
  programs.starship = {
    enable = true;
    enableFishIntegration = true;
  };

  home.shell.enableNushellIntegration = true;

  programs.nushell = {
    enable = true;
    configFile.source = ./config.nu;
    envFile.source = ./env.nu;

    extraConfig = ''
      source ${./completions/cargo.nu}
      source ${./completions/git.nu}
      source ${./completions/generic.nu}
    '';
  };
}
