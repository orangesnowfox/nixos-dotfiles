{ config, pkgs, ... }:

{
  programs.gpg = {
    enable = true;
  };

  services.gpg-agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-qt;
  };

  services.playerctld.enable = true;
  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  programs.eza.enable = true;

  programs.firefox = {
    enable = true;
    package = (pkgs.wrapFirefox (pkgs.firefox-unwrapped.override { pipewireSupport = true;}) {});
  };

  programs.git = {
    enable = true;
    userName = "Skyler Rain Ross";
    userEmail = "orangesnowfox@gmail.com";
    delta = {
      enable = true;
      options = {
        plus-style = "syntax #012800";
        minus-style = "syntax #340001";
        syntax-theme = "Monokai Extended";
        navigate = true;
        line-numbers = true;
      };
    };
    extraConfig = {
      init = {
        defaultBranch = "main";
      };

      pull = {
        rebase = true;
      };
    };

    ignores = [
      ".idea"
      ".vscode"
      "*.code-workspace"
      ".cargo-ok"
      "*.session"
      ".direnv"
      "*.envrc"
    ];

    includes = [
      {
        condition = "gitdir:~/ghq/gitlab.com-lb/**";
        contents.user = {
          email = "skyler@launchbadge.com";
          name = "Skyler Ross";
          signingKey = "41EC25DD66F0B33E";
        };
      }
      {
        condition = "gitdir:~/ghq/github.com-lb/**";
        contents.user = {
          email = "skyler@launchbadge.com";
          name = "Skyler Ross";
          signingKey = "41EC25DD66F0B33E";
        };
      }
    ];

    signing = {
      key = "F0FA59ACC62ACF43";
      signByDefault = true;
    };
  };


  home.packages = [
    pkgs.bat
    pkgs.ripgrep
    pkgs.git-crypt
    pkgs.gnupg
    pkgs.nixpkgs-fmt
    pkgs.neofetch
    pkgs.vesktop
    pkgs.revolt-desktop
    pkgs.spotify
    pkgs.xorg.xkill
    pkgs.pamixer
    pkgs.playerctl
    pkgs.killall
    pkgs.units
    pkgs.plover.dev
    pkgs.xfce.thunar
    pkgs.slack
    pkgs.tor-browser-bundle-bin
    pkgs.my.anki-beta-bin
    pkgs.mpv
    pkgs.rivalcfg
    pkgs.my.iamb-desktop
    # pkgs.my.djot
    # pkgs.my.calc-synergism-save
  ];

  programs.chromium = {
    enable = true;
    package = pkgs.ungoogled-chromium;
  };

  # todo: break-out
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    extensions = with pkgs.vscode-extensions; [
      # broken on nixpkgs master
      # matklad.rust-analyzer
      # dracula-theme.theme-dracula
      # tamasfe.even-better-toml
      # jnoortheen.nix-ide
      # mikestead.dotenv
      # gruntfuggly.todo-tree
      # arrterian.nix-env-selector
      # tabnine.tabnine-vscode
    ];

    keybindings = [
      {
        key = "ctrl+r";
        command = "editor.action.rename";
        when = "editorHasRenameProvider && editorTextFocus && !editorReadonly";
      }
    ];
  };

  home.sessionVariables = {
    EDITOR = "${pkgs.neovim}/bin/nvim";
    NIXOS_OZONE_WL = "1";
  };

  # hack this should just work and not need to exist.
  programs.nushell.extraEnv = ''
    $env.EDITOR = "${pkgs.neovim}/bin/nvim";
    $env.NIXOS_OZONE_WL = 1;
  '';

  # Yup, gotta set them here too.
  systemd.user.sessionVariables = {
    EDITOR = "${pkgs.neovim}/bin/nvim";
    NIXOS_OZONE_WL = "1";
  };
}
