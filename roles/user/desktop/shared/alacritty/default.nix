{ pkgs, config, lib, ... }:
{
    home.sessionVariables = {
        TERMINAL = "${pkgs.alacritty}/bin/alacritty";
    };

    home.packages = [
        pkgs.alacritty
    ];

    home.file = {
        ".config/alacritty/alacritty.toml".source = ./alacritty.toml;
        ".config/alacritty/themes".source = ./themes;
    };
}
