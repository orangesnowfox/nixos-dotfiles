{ pkgs, config, lib, ... }:
{

  fonts.fontconfig.enable = true;
  home.packages = [
    pkgs.jetbrains-mono
    pkgs.font-awesome
    pkgs.dejavu_fonts
    pkgs.victor-mono
    pkgs.rictydiminished-with-firacode
    pkgs.noto-fonts-cjk-sans
    pkgs.noto-fonts-cjk-serif
    pkgs.anonymousPro
    pkgs.cantarell-fonts
  ];
}
