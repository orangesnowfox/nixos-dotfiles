# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";
  # virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;
  environment.systemPackages = [ pkgs.virt-manager pkgs.glances ];

  services.freshrss = {
    enable = true;
    baseUrl = "http://localhost";
    virtualHost = "freshrss";
    # lol (we're localhost)
    passwordFile = pkgs.writeText "password" "secret";
  };

  # overwrite the port so that we have the preferred ports reserved.
  services.nginx.virtualHosts.freshrss.listen = [{
    addr = "localhost";
    port = 8004;
  }];

  # todo: put this elsewhere
  hardware.keyboard.zsa.enable = true;
  hardware.opentabletdriver.enable = true;
}
