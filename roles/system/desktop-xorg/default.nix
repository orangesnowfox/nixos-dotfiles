{ config, pkgs, ... }:

{
  environment.systemPackages = [
    pkgs.grim
  ];

  xdg.portal = {
    wlr.enable = true;
    config.common = {
      default = ["wlr" "gtk"];
    };

    extraPortals = [
      pkgs.xdg-desktop-portal-gtk
    ];

    enable = true;
  };

  services.displayManager = {
    sessionPackages = [ pkgs.sway ];
    defaultSession = "sway";
    enable = true;
    sddm = {
      enable = true;
      wayland.enable = true;
    };
    logToJournal = true;
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;

    displayManager = {
      # lightdm.enable = true;

      session = [ 
        {
          manage = "desktop";
          name = "xsession";
          start = "exec $HOME/.xsession";
        }
      ];
    };

    dpi = 192;
    desktopManager.xterm.enable = true;
  };

  services.libinput = {
    enable = true;
    # disable mouse/touchpad acceleration
    mouse.accelProfile = "flat";
    touchpad.accelProfile = "flat";
  };

  environment.variables = {
    GDK_SCALE = "2";
    GDK_DPI_SCALE = "2";
    NIXOS_OZONE_WL = "1";
    _JAVA_OPTIONS = "-Dsun.java2d.uiScale=2";
  };

  security.rtkit.enable = true;
  security.polkit.enable = true;

  # sigh
  services.gnome.at-spi2-core.enable = true;
  programs.dconf.enable = true;

  services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse.enable = true;
    extraConfig.pipewire = {
      "99-custom" = {
        "context.properties" = {
          "link.max-buffers" = 16;
          "default.clock.rate" = 48000;
          "default.clock.quantum" = 32;
          "default.clock.min-quantum" = 32;
          "default.clock.max-quantum" = 64;
          "module.x11.bell" = false;
        };

      };
    };
    extraConfig.pipewire-pulse = {
      "99-custom" = {
        "stream.properties" = {
            "node.latency" = "32/48000";
            "resample.quality" = 1;
        };
        "pulse.properties" = {
            "pulse.min.req" =       "32/48000";
            "pulse.default.req" =   "32/48000";
            "pulse.max.req" =       "32/48000";
            "pulse.min.quantum" =   "32/48000";
            "pulse.max.quantum" =   "32/48000";
        };
      };
    };

    wireplumber.extraConfig."99-alsa-lowlatency" = {
      "monitor.alsa.rules" = [
        {
          matches = [
            { "node.name" = "~alsa_output.*"; }
          ];
          actions = {
            update-props = {
              "audio.format" = "S32LE";
              "audio.rate" = "96000";
              "api.alsa.period-size" = 2; # default: 1024, trial and error.
            };
          };
        }
      ];
    };
  };

  services.gnome.gnome-keyring.enable = true;
  services.accounts-daemon.enable = true;
  environment.pathsToLink = [ "/share/accountsservice" ];
}
